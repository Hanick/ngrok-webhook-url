﻿using LaunchCMDs;

Cmd cmd = new();
GetDataFromFile getDataFromUrl = new();
GetFileFromUrl getFileFromUrl = new();
OpenConfigFile openConfigFile = new();
OpenUrl openUrl = new();

//Launch Ngrok CMD with parameters
cmd.Launch(CommandsForCmd.NgrokFile, CommandsForCmd.NgrokCommand);

//Get and save xml file form http://localhost:4040/api/tunnels to json format
getFileFromUrl.Save();

//Open json file and get the https weebhook link 
var url = getDataFromUrl.OpenFileAndGetUrl();

//Open and edit config file, just paste in the file new ngrok webhook url 
openConfigFile.EditConfigFile(url);

//Open api/bot url to push weebhook to the Telegram API
openUrl.BotUrl();