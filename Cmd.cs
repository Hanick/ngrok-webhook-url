﻿using System.Diagnostics;

namespace LaunchCMDs;

public class Cmd
{
    public void Launch(string fileName, string command)
    {
        Process process = new();
        ProcessStartInfo startInfo = new()
        {
            FileName = fileName,
            Arguments = command,
            WindowStyle = ProcessWindowStyle.Hidden
        };
        process.StartInfo = startInfo;
        Process.Start(startInfo);
    }
}