﻿namespace LaunchCMDs;
public class OpenConfigFile
{
    public void EditConfigFile(string url)
    {
        var json = File.ReadAllText("C:\\MOE\\appsettings.json");
        dynamic jsonObj = Newtonsoft.Json.JsonConvert.DeserializeObject(json);
        jsonObj["BotSettings"]["WebHook"] = url;
        string output = Newtonsoft.Json.JsonConvert.SerializeObject(jsonObj, Newtonsoft.Json.Formatting.Indented);
        File.WriteAllText("C:\\MOE\\appsettings.json", output);
    }
}