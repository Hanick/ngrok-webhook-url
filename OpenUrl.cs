﻿using System.Diagnostics;

namespace LaunchCMDs;

public class OpenUrl
{
    public void BotUrl()
    {
        Process process = new();
        ProcessStartInfo startInfo = new()
        {
            FileName = "C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe",
            Arguments = "http://localhost:8082/api/bot",
            WindowStyle = ProcessWindowStyle.Hidden
        };
        process.StartInfo = startInfo;
        Process.Start(startInfo);
    }
}