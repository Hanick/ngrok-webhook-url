﻿using Newtonsoft.Json;

namespace LaunchCMDs;

public class GetDataFromFile
{
    public string OpenFileAndGetUrl()
    {
        var jsonFile = File.ReadAllText(@"C:\tunnels.json");
        dynamic jsonObj = JsonConvert.DeserializeObject(jsonFile);
        return jsonObj["tunnels"][0]["public_url"].ToString();
    }
}